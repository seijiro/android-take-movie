package test.preview.movie;

import java.io.File;
import java.io.IOException;
import java.util.List;

import android.app.Activity;
import android.hardware.Camera;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.os.Environment;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class MovieAppActivity
extends Activity
{
  //サーフェイスビュー
  private SurfaceView mPreview;
  //レコーダー
  private MediaRecorder mRecorder;

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState)
  {
    super.onCreate( savedInstanceState );
    // フルスクリーン表示にします
    getWindow().addFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    // タイトルバーを非表示にします
    requestWindowFeature(Window.FEATURE_NO_TITLE);

    setContentView( R.layout.main );

    //プレビュー用の情報初期化
    mPreview = (SurfaceView)findViewById(R.id.camera_view);
    SurfaceHolder holder = mPreview.getHolder();
    holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
  }

  public void onStartButton(View view){
    //レコーダ生成
    mRecorder = new MediaRecorder();
    //レコーダをビデオカメラにセット
    mRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
    // オーディオ入力にマイクをセット
    mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
    // 出力フォーマットに3gpをセット
    mRecorder.setOutputFormat(
            MediaRecorder.OutputFormat.THREE_GPP);

    // 出力ファイルパスを作る
    String path = createFilePath();
    // セット
    mRecorder.setOutputFile(path);
    // フレームレートをセット
    mRecorder.setVideoFrameRate(15);

    //カメラの解像度を取得するために一度カメラを生成
    Camera camera = Camera.open();
    //パラメータ取得
    Camera.Parameters param = camera.getParameters();
    final List< Camera.Size > supportedResolution = param
      .getSupportedPreviewSizes();
    Camera.Size size = supportedResolution.get( 0 );
    //カメラを閉じる
    camera.release();
    camera = null;
    // 撮影サイズを設定
    mRecorder.setVideoSize(size.width, size.height);
    // ビデオエンコーダーにMPEG_4_SPをセット
    mRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
    // オーディオエンコーダーにAMR_NBをセット
    mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

    // プレビュー表示にSurfaceをセット
    mRecorder.setPreviewDisplay(
            mPreview.getHolder().getSurface());
    try {
        // 準備して
        mRecorder.prepare();
    } catch (IllegalStateException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
    // 録画スタート！
    mRecorder.start();

  }
  public void onStopButton(View view){
    // 録画を終了
    mRecorder.stop();
    // レコーダーを解放
    mRecorder.release();
  }

  private String createFilePath() {
    // SDカードのディレクトリ
    final File dir = Environment.getExternalStorageDirectory();
    // アプリ名でディレクトリ
    final File appDir = new File(dir, "MovieApp");
    // ディレクトリを作る
    if (!appDir.exists()) appDir.mkdir();
    // ファイル名
    final String name = System.currentTimeMillis() + ".3gp";
    // 出力ファイルのパス
    return new File(appDir, name).getAbsolutePath();
  }
}
